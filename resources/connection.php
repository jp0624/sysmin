<?php 
	print "connection file running";
    require_once("config.php");

    if (!$connection) {
 	   $connection = mysqli_connect($config[db][host],$config[db][username],$config[db][password],$config[db][dbname]);
 	}

    if ($connection->connect_error) {
        echo 'no connection';
        die("Connection failed: " . $connection->connect_error);
    }
?>