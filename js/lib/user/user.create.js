var sysmin = sysmin || {};

sysmin.forms = {};

sysmin.forms.createUser = {
	error: {
		elem: 		$("#msgResponse"),
		val: 		''
	},
	username : {
		elem: 		$("#username"),
		val: 		$("#username").val(),
		unique: 	false,
		checked: 	false
	},
	email : {
		elem: 		$("#email"),
		val: 		$("#email").val(),
		valid: 		false,
		checked: 	false
	},
	password : {
		initial: {
			elem: 	$("#password-initial"),
			val: 	$("#password-initial").val()
		},
		confirm: {
			elem: 	$("#password-confirm"),
			val: 	$("#password-confirm").val()
		}
	}
};


$(document).ready(function() {

	$("#submit").click(function() {


		var _thisForm = sysmin.forms.createUser;


		_thisForm.username.val			= $("#username").val();
		_thisForm.email.val				= $("#email").val();
		_thisForm.password.initial.val 	= $("#password-initial").val();
		_thisForm.password.confirm.val 	= $("#password-confirm").val();
		_thisForm.error.val 			= '';

		console.log('form submit start');

		console.log('password initial: ', _thisForm.password.initial.val);
		console.log('password confirm: ', _thisForm.password.confirm.val);

		if (_thisForm.username.val == '' || _thisForm.email.val == '' || _thisForm.password.initial.val == '' || _thisForm.password.confirm.val == '') {
			console.warn('EMPTY FIELDS');
			_thisForm.error.val = '<p>Some of the fields were not completed</p>';
		}

		checkUsername(_thisForm);
		if(!_thisForm.username.unique || _thisForm.username.val == ''){
			console.warn('USERNAME NOT UNIQUE');
			_thisForm.error.val += '<p>Invalid username!</p>';
		}

		checkEmail(_thisForm);
		if(!_thisForm.email.valid){
			console.warn('EMAIL NOT VALID');
			_thisForm.error.val += '<p>This Email is not valid!</p>';

		}
		
		if(_thisForm.error.val == '' ){
			console.log('NO ERRORS FOUND');
			$("#msgResponse").removeClass('error').html(_thisForm.error.val);
			//_thisForm.error.elem.removeClass('error').html(_thisForm.error.val);
		}else{
			console.warn('ERRORS FOUND');
			console.log(_thisForm.error.val);

			$("#msgResponse").addClass('error').html(_thisForm.error.val);
			//_thisForm.error.elem.addClass('error').html(_thisForm.error.val);
			return;
		};

		createUser(_thisForm);

	});

	function createUser(_thisForm){
		console.warn('CREATE USER');

		var frmSubmit = $.ajax({
			type: "POST",
        	async: false,
			dataType: 'html',
			url: "/php/lib/user/create.php",
			data: {
				username: _thisForm.username.val,
				email: _thisForm.email.val,
				password: _thisForm.password.initial.val
			},
			error: function(XMLHttpRequest, textStatus, errorThrown){

			  	console.log(XMLHttpRequest);
			  	console.log('textStatus: ' + textStatus);
			  	console.log('errorThrown: ' + errorThrown);

			},
			success: function(data) {

				//var userCreated = $.parseJSON(data);
				var userCreated = data;
				console.log('User Created: ', userCreated);

			}
		});

	}
	function checkEmail(_thisForm){
		var frmSubmit = $.ajax({
			type: "POST",
        	async: false,
			dataType: 'json',
			url: "/php/lib/user/check.email.php",
			data: {
				email: _thisForm.email.val
			},
			success: function(data) {

				_thisForm.email.valid = $.parseJSON(data);
				_thisForm.email.checked = true;
				console.log('email.valid: ', _thisForm.email.valid);

				if(_thisForm.email.valid){

					_thisForm.email.elem.removeClass('error');
				} else {

					_thisForm.email.elem.addClass('error');
				}
			}
		});

	}

	function checkUsername(_thisForm){
		var frmSubmit = $.ajax({
			type: "POST",
        	async: false,
			dataType: 'json',
			url: "/php/lib/user/check.username.php",
			data: {
				username: _thisForm.username.val
			},
			success: function(data) {

				_thisForm.username.unique = $.parseJSON(data);
				_thisForm.username.checked = true;

				console.log('usernameUnique: ', _thisForm.username.unique);

				if(_thisForm.username.unique){

					_thisForm.username.elem.removeClass('error');
				} else {

					_thisForm.username.elem.addClass('error');
				}
			}
		});
		 
	}

});