<!DOCTYPE html>
<html>
    <?php
        require_once("/resources/config.php");
	?>

    <head>
        <meta charset="utf-8">
        <title>Sinking Ship - Sysmin Login</title>
		
		<script src="js/jquery-1.11.1.min.js"></script>
		<script src="js/lib/user/user.login.js"></script>

	    <link href="css/reset.min.css" rel="stylesheet" type="text/css" />
	    <link href="css/base.css" rel="stylesheet" type="text/css" />
	    <link href="css/sysmin.css" rel="stylesheet" type="text/css" />
    </head>
    <body class="page page-login">
        <div id="pg-wrapper">
            <h1>Sinking Ship Admin System</h1>
            <article>
                <h2>Session Data</h2>
                <section>
	        		<?php
		        		printf("<p>");
			        		printf("session (id): $s" . $_SESSION['userid']);
					        printf("<br>session (username): $s" . $_SESSION['username']);
					        printf("<br>session (password): $s" . $_SESSION['password']);
				        printf("</p>");
			        ?>
		        </section>
            </article>
            <article>
                <h2>User Login Form</h2>
                <section>
					<form id="loginUser" name="loginUser">
						<div id="msgResponse">

						</div>
						<dl>
							<dt class="username">
								<label for="username">Username:</label>
							</dt>
							<dd>
								<input name="username" id="username" type="text" value="test">
							</dd>
							<dt class="password">
								<label for="password">Password:</label>
							</dt>
							<dd>
								<input name="password" id="password" type="password" value="test">
							</dd>
							<dt class="submit">
								<button type="button" id="submit">Login</button>
							</dt>
						</dl>
					</form>
				</section>
			</article>
		</div>
    </body>
</html>