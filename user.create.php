<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <title>Sinking Ship - Sysmin User Create</title>
		
		<script src="js/jquery-1.11.1.min.js"></script>
		<script src="js/lib/user/user.create.js"></script>

	    <link href="css/reset.min.css" rel="stylesheet" type="text/css" />
	    <link href="css/base.css" rel="stylesheet" type="text/css" />
	    <link href="css/sysmin.css" rel="stylesheet" type="text/css" />
    </head>
    <body class="page page-create">
        <div id="pg-wrapper">
            <h1>Sinking Ship Admin System</h1>

            <article>
                <h2>User Creation Form</h2>
	                <section>
						<form id="createUser" name="createUser">
							<div id="msgResponse">

							</div>
							<dl>
								<dt class="username">
									<label for="username">Username:</label>
								</dt>
								<dd>
									<input name="username" id="username" type="text" value="username">
								</dd>
								<dt class="email">
									<label for="email">Email:</label>
								</dt>
								<dd>
									<input name="email" id="email" type="text" value="DEFAULT@EMAIL.COM">
								</dd>
								<dt class="password password-initial">
									<label for="password-initial">Password:</label>
								</dt>
								<dd>
									<input name="password-initial" id="password-initial" type="password" value='test'>
								</dd>
								<dt class="password password-confirm">
									<label for="password-confirm">Confirm Password:</label>
								</dt>
								<dd>
									<input name="password-confirm" id="password-confirm" type="password" value='test'>
								</dd>
								<dt class="submit">
									<button type="button" id="submit">Create User</button>
								</dt>
							</dl>
						</form>
                </section>
			</article>
		</div>
    </body>
</html>