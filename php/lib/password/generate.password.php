<?php

    function generatePassword($password){
        $cost = 10;
        $salt = strtr(base64_encode(mcrypt_create_iv(16, MCRYPT_DEV_URANDOM)), '+', '.');
        $salt = sprintf("$2a$%02d$", $L) . $salt;

        $options = [
            'cost' => $cost,
            'salt' => $salt,
        ];
        
        $password = password_hash($password, PASSWORD_BCRYPT, $options)."\n";

        return $password;
    }
    
?>