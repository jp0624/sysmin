<html>
    <?php
        require_once("../../resources/config.php");

        $connection = mysqli_connect($config[db][host],$config[db][username],$config[db][password],$config[db][dbname]);

        if($connection === false) {
            console.log('Connection to DB failed from html');
            die("+connection failed: " . $connection->connect_error);
        } else {
            console.log('Connection to DB success from html');
        }

    ?>
<head>
<title>User Info</title>
</head>
<body>
<?php

$query = file_get_contents( '../../queries/users/all.txt');

$response = @mysqli_query($connection, $query);

if ($response)
{
    echo '
        <div><table align="left" cellspacing="5" cellpadding="8">
            <tr>
                <td align="left"><b>Username</b></td>
                <td align="left"><b>Password</b></td>
                <td align="left"><b>Email</b></td>
            </tr>';

    while ($row = mysqli_fetch_array($response))
    {
        echo '<tr>' .
            '<td align="left">' . $row['username'] . '</td>' .
            '<td align="left">' . $row['password'] . '</td>' .
            '<td align="left">' . $row['email'] . '</td>' .
            '</tr>';
    }

    echo '</table></div>';
}
else
{
    echo 'Couldn\'t issue database query';

    echo mysqli_error($connection);
}

mysqli_close($connection);

?>
<div>
<!-- <form action="redirect.php" method="post">
<p><b> Go Back </b></p>
<p>
    <input type="submit" name="redirect" value="Back" />
</p>
</form> -->
</div>
</body>
</html>