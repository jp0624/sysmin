var sysmin = sysmin || {};

sysmin.forms = {};

sysmin.forms.loginUser = {
	error: {
		elem: 		$("#msgResponse"),
		val: 		''
	},
	username : {
		elem: 		$("#username"),
		val: 		$("#username").val(),
		unique: 	false,
		checked: 	false
	},
	password 		: {
		elem: 		$("#password"),
		val: 		$("#password").val(),
		valid: 		'false'
	}
};


$(document).ready( function() {

	$("#submit").on('click', function() {

		var _thisForm = sysmin.forms.loginUser;
		
		_thisForm.username.val		= $("#username").val();
		_thisForm.username.unique	= false;
		_thisForm.username.checked	= false;
		_thisForm.password.val 		= $("#password").val();
		_thisForm.password.valid	= false;
		_thisForm.error.val 		= '';

		console.log('form submit start');


		if (_thisForm.username == '' || _thisForm.email == '' || _thisForm.passwordInitial == '' || _thisForm.passwordConfirm == '') {
			console.warn('EMPTY FIELDS');
			_thisForm.error.val = '<p>Some of the fields were not completed</p>';
		}

		checkUsername(_thisForm);
		if(_thisForm.username.unique){
			console.warn('INVALID USERNAME');
			_thisForm.error.val += '<p>Username or password Invalid!</p>';
		}

		// Dont check password, but send out error, if not empty
		if(!_thisForm.error.val == '' ){

			console.log('ERRORS FOUND');
			console.log(_thisForm.error.val);

			$("#msgResponse").addClass('error').html(_thisForm.error.val);
			return;
		}

		checkPassword(_thisForm);
		if(!_thisForm.password.valid){
			console.warn('INVALID PASSWORD');
			_thisForm.error.val += '<p>Username or password Invalid!</p>';
		}

		// Send out error, if not empty
		if(!_thisForm.error.val == '' ){
			console.log('ERRORS FOUND');
			console.log(_thisForm.error.val);

			$("#msgResponse").addClass('error').html(_thisForm.error.val);
			return;
		}

	});

	function loginUser(_thisForm){

		var frmSubmit = $.ajax({
			type: "POST",
        	async: false,
			dataType: 'html',
			url: "/php/lib/user/login.php",
			data: {
				username: _thisForm.username.val,
				email: _thisForm.email.val,
				password: _thisForm.passwordInitial
			},
			error: function(XMLHttpRequest, textStatus, errorThrown){

			  	console.log(XMLHttpRequest);
			  	console.log('textStatus: ' + textStatus);
			  	console.log('errorThrown: ' + errorThrown);

			},
			success: function(data) {

				//var userCreated = $.parseJSON(data);
				var userCreated = data;
				console.log('User Created: ', userCreated);

			}
		});

	}

	function checkUsername(_thisForm){
		var frmSubmit = $.ajax({
			type: "POST",
        	async: false,
			dataType: 'json',
			url: "/php/lib/user/check.username.php",
			data: {
				username: _thisForm.username.val
			},
			success: function(data) {

				_thisForm.username.unique = $.parseJSON(data);
				_thisForm.username.checked = true;
				console.log('usernameUnique: ', _thisForm.username.unique);

				if(_thisForm.username.unique){

					_thisForm.username.elem.removeClass('error');
				} else {

					_thisForm.username.elem.addClass('error');
				}
			}
		});
		 
	};

	function checkPassword(_thisForm){
		var frmSubmit = $.ajax({
			type: "POST",
        	async: false,
			dataType: 'html',
			url: "/php/lib/user/check.password.php",
			data: {
				username: _thisForm.username.val,
				password: _thisForm.password.val
			},
			error: function(XMLHttpRequest, textStatus, errorThrown){

			  	console.log(XMLHttpRequest);
			  	console.log('textStatus: ' + textStatus);
			  	console.log('errorThrown: ' + errorThrown);
			},
			success: function(data) {
				var _thisForm = jpsite.forms.loginUser;

				_thisForm.password.valid = data;
				
				console.log('CHECK PASSWORD SUCCESS');
				console.log('Data back: ', data);

			}
		});
		 
	};



});